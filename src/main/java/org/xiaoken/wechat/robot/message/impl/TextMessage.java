package org.xiaoken.wechat.robot.message.impl;

import com.alibaba.fastjson.annotation.JSONField;
import org.xiaoken.wechat.robot.message.Message;

import java.util.List;

/**
 * 普通文本消息
 * @author guokenye
 */
public class TextMessage implements Message {

    private String content;
    @JSONField(name = "mentioned_list")
    private List<String> mentionedList;
    @JSONField(name = "mentioned_mobile_list")
    private List<String> mentionedMobileList;


    public TextMessage() {
    }

    public TextMessage(String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getMentionedList() {
        return mentionedList;
    }

    public void setMentionedList(List<String> mentionedList) {
        this.mentionedList = mentionedList;
    }

    public List<String> getMentionedMobileList() {
        return mentionedMobileList;
    }

    public void setMentionedMobileList(List<String> mentionedMobileList) {
        this.mentionedMobileList = mentionedMobileList;
    }

    @Override
    public String getMsgType() {
        return Type.TEXT;
    }

    @Override
    public Object getMsgContent() {
        return this;
    }


}
