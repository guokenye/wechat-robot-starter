package org.xiaoken.wechat.robot.message;

/**
 * @program: wechat-robot-starter
 * @description: 消息异常
 * @author: guokenye
 **/
public class MessageException extends Exception {

    private Integer code;

    public MessageException() {
    }

    public MessageException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
