package org.xiaoken.wechat.robot.message.impl;

import cn.hutool.core.util.StrUtil;

/**
 * 一个简单得makrdown字符串构建工具
 *
 * @author guokenye
 */
public class MarkDownBuilder {

    public static final String TITLE_PREFIX = "#";
    public static final String BLANK = " ";
    public static final String BOLD = "**";
    public static final String QUOTE = "> ";
    public static final String INFO_FORMAT = "<font color='info'>%s</font>";
    public static final String COMMENT_FORMAT = "<font color='comment'>%s</font>";
    public static final String WARNING_FORMAT = "<font color='warning'>%s</font>";
    public static final String LINK_FORMAT = "[%s](%s)";
    public static final String NEW_LINE = "\n";

    private StringBuilder sb;

    public MarkDownBuilder() {
        this.sb = new StringBuilder();
    }

    public MarkDownBuilder(String str) {
        this();
        this.sb.append(str);
    }

    /**
     * 添加普通字符串
     *
     * @param str
     * @return
     */
    public MarkDownBuilder addStr(String str) {
        this.sb.append(str);
        return this;
    }

    /**
     * 添加标题
     *
     * @param title
     * @param level
     * @return
     */
    public MarkDownBuilder addTitle(String title, int level) {
        String prefix = StrUtil.padAfter(TITLE_PREFIX, level, TITLE_PREFIX);
        this.sb.append(prefix).append(BLANK).append(title);
        return this;
    }

    /**
     * 加粗
     *
     * @param str
     * @return
     */
    public MarkDownBuilder addBold(String str) {
        this.sb.append(BOLD).append(str).append(BOLD);
        return this;
    }

    /**
     * 添加链接
     *
     * @param linkText
     * @param url
     * @return
     */
    public MarkDownBuilder addLink(String linkText, String url) {
        String str = String.format(LINK_FORMAT, linkText, url);
        return addStr(str);
    }

    /**
     * 添加引用
     *
     * @param str
     * @return
     */
    public MarkDownBuilder addQuote(String str) {
        this.sb.append(QUOTE).append(str);
        return this;
    }

    /**
     * 添加一般信息
     *
     * @param str
     * @return
     */
    public MarkDownBuilder addInfoStr(String str) {
        String format = String.format(INFO_FORMAT, str);
        return addStr(format);
    }

    /**
     * 添加评论信息
     *
     * @param str
     * @return
     */
    public MarkDownBuilder addCommentStr(String str) {
        String format = String.format(COMMENT_FORMAT, str);
        return addStr(format);
    }

    /**
     * 添加警告信息
     *
     * @param str
     * @return
     */
    public MarkDownBuilder addWarningStr(String str) {
        String format = String.format(WARNING_FORMAT, str);
        return addStr(format);
    }

    /**
     * 换行
     *
     * @return
     */
    public MarkDownBuilder addNewLine() {
        return addStr(NEW_LINE);
    }

    /**
     * 生成最终字符串
     *
     * @return
     */
    public String genStr() {
        return this.sb.toString();
    }

    /**
     * 清除所有字符串
     * @return
     */
    public MarkDownBuilder deleteAll() {
        this.sb.delete(0, this.sb.length() - 1);
        return this;
    }
}

