package org.xiaoken.wechat.robot.message.impl;

import org.xiaoken.wechat.robot.message.Message;

/**
 * makrdown类型消息
 * @author guokenye
 */
public class MarkDownMessage implements Message {

    private MarkDownBuilder builder;

    public MarkDownMessage() {
        this(new MarkDownBuilder());
    }

    public MarkDownMessage(MarkDownBuilder builder) {
        this.builder = builder;
    }

    public MarkDownBuilder getBuilder() {
        return builder;
    }

    public void setBuilder(MarkDownBuilder builder) {
        this.builder = builder;
    }

    @Override
    public String getMsgType() {
        return Type.MARKDOWN;
    }

    @Override
    public Object getMsgContent() {
        return wrapContent(builder.genStr());
    }
}
