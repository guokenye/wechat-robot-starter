package org.xiaoken.wechat.robot.message;

import java.io.IOException;

/**
 * @program: wechat-robot-starter
 * @description: 企业微信机器人消息发送者接口
 * @author: guokenye
 * @create: 2020-05-19 00:59
 **/
public interface Sender {
    /**
     * 发送消息
     * @param message
     * @throws IOException 网络异常
     * @throws MessageException 消息异常
     */
    void send(Message message) throws IOException,MessageException;

}
