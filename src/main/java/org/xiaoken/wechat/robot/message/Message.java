package org.xiaoken.wechat.robot.message;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 企业微信机器人消息定义
 * @author guokenye
 */
public interface Message extends Serializable {
    /**
     * 消息类型
     */
    final class Type implements Serializable {
        public static final String TEXT = "text";
        public static final String MARKDOWN = "markdown";
    }

    /**
     * 对content提供一个包装得方法
     *
     * @param target
     * @return
     */
    default Map<String, Object> wrapContent(Object target) {
        Map<String, Object> map = new HashMap<>(1);
        map.put("content", target);
        return map;
    }

    /**
     * 获取消息类型
     *
     * @return
     */
    @JSONField(serialize = false)
    String getMsgType();

    /**
     * 获取最终消息
     *
     * @return
     */
    @JSONField(serialize = false)
    Object getMsgContent();
}
