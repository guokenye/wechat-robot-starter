package org.xiaoken.wechat.robot.message.impl;

import freemarker.template.TemplateException;
import org.xiaoken.wechat.robot.message.Message;
import org.xiaoken.wechat.robot.util.TemplateUtil;

import java.io.IOException;

/**
 * markdown消息类型，支持模板类
 * 使用模板是为了在对应复杂消息的时候，用模板+占位符的方式去渲染消息，而不是直接靠字符串拼接
 * 在推送报警西悉尼、日报信息的时候很方便，随时修改模板，就可以修改推送的样式
 *
 * @author guokenye
 */
public class MarkDownTemplateMessage implements Message {

    private Object param;
    private String templateName;
    private String content;

    public MarkDownTemplateMessage(Object param, String templateName) throws IOException, TemplateException {
        this.param = param;
        this.templateName = templateName;
        this.renderContent();
    }

    private void renderContent() throws IOException, TemplateException {
        this.content = TemplateUtil.renderTemplate(templateName, param)
                .replaceAll("\\r\\n[ \\t\\x0B\\f\\r]*", "\n");
    }


    @Override
    public String getMsgType() {
        return Type.MARKDOWN;
    }

    @Override
    public Object getMsgContent() {
        return wrapContent(this.content);
    }


}
