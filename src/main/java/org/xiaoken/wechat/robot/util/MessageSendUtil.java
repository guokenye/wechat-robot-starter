package org.xiaoken.wechat.robot.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.xiaoken.wechat.robot.message.Message;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * 消息发送工具类
 *
 * @author guokenye
 */
public class MessageSendUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageSendUtil.class);
    private static final String MESSAGE_TYPE_FIELD = "msgtype";
    /**
     * 默认socket超时时间(毫秒)
     */
    public static final Integer DEFAULT_SOCKET_TIMEOUT = 5000;
    /**
     * 默认connect超时时间(毫秒)
     */
    public static final Integer DEFAULT_CONNECT_TIMEOUT = 10000;


    public static String send(Message message, String webhook) throws IOException {
        return send(message, webhook, DEFAULT_SOCKET_TIMEOUT, DEFAULT_CONNECT_TIMEOUT);
    }

    public static String send(Message message, String webhook, Integer timeout) throws IOException {
        return send(message, webhook, timeout, timeout);
    }

    public static String send(Message message, String webhook, Integer socketTimeout, Integer connectTimeout) throws IOException {
        String jsonStr = genMsgText(message);
        return send(jsonStr, webhook, socketTimeout, connectTimeout);
    }

    /**
     * 发送消息
     * @param message
     * @param webhook
     * @param socketTimeout
     * @param connectTimeout
     * @return
     * @throws IOException
     */
    public static String send(String message, String webhook, Integer socketTimeout, Integer connectTimeout) throws IOException {
        if (StrUtil.isBlank(message)) {
            throw new IllegalArgumentException("message is null or blank!");
        }
        if (StrUtil.isBlank(webhook)) {
            throw new IllegalArgumentException("webhook is null or blank!");
        }

        socketTimeout = socketTimeout == null ? DEFAULT_SOCKET_TIMEOUT : socketTimeout;
        connectTimeout = connectTimeout == null ? DEFAULT_CONNECT_TIMEOUT : connectTimeout;

        LOGGER.debug("send \n message {} to \n webhook {}", message, webhook);
        String resultStr = Request.Post(webhook)
                .bodyString(message, ContentType.APPLICATION_JSON)
                .socketTimeout(socketTimeout)
                .connectTimeout(connectTimeout)
                .execute().returnContent().asString();
        LOGGER.debug("return message {}", resultStr, webhook);
        return resultStr;
    }


    /**
     * 生成指定得msg格式字符串
     * {msgtype；xxx,content:xxx}
     *
     * @param message
     * @return
     */
    private static String genMsgText(Message message) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(MESSAGE_TYPE_FIELD, message.getMsgType());
        jsonObject.put(message.getMsgType(), message.getMsgContent());
        return jsonObject.toString(SerializerFeature.PrettyFormat);
    }

}
