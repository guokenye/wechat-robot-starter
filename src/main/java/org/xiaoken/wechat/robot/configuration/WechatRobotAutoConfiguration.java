package org.xiaoken.wechat.robot.configuration;

import cn.hutool.core.collection.CollectionUtil;
import org.xiaoken.wechat.robot.message.impl.GenericSender;
import org.xiaoken.wechat.robot.util.TemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.net.URISyntaxException;

/**
 * 机器人自动配置
 *
 * @author guokenye
 */
@Configuration
@EnableConfigurationProperties(WeChatRobotProperties.class)
@ConditionalOnProperty(name = "wechat.robot.enable", havingValue = "true")
public class WechatRobotAutoConfiguration {


    @Autowired
    private WeChatRobotProperties properties;
    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    public void init() throws URISyntaxException {
        //注册机器人实例
        registerRobotInstance();
        //配置模板
        TemplateUtil.setConfiguration(getTemplateConfiguration());
    }


    private void registerRobotInstance() {
        // 获取BeanFactory
        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) applicationContext
                .getAutowireCapableBeanFactory();
        //配置多个机器人实例
        if (CollectionUtil.isNotEmpty(properties.getSenderMap())) {
            properties.getSenderMap().forEach((k, v) -> {
                BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(GenericSender.class);
                builder.addConstructorArgValue(k);
                builder.addConstructorArgValue(v);
                builder.addConstructorArgValue(properties.getSocketTimeout());
                builder.addConstructorArgValue(properties.getConnectTimeout());
                defaultListableBeanFactory.registerBeanDefinition(getBeanName(k, properties.getSenderNameSuffix()), builder.getBeanDefinition());
            });
        }
    }


    private String getBeanName(String instanceName, String instanceNameSuffix) {
        return instanceName + instanceNameSuffix;
    }


    private freemarker.template.Configuration getTemplateConfiguration() throws URISyntaxException {
        // 创建配置类
        freemarker.template.Configuration configuration = new freemarker.template.Configuration(freemarker.template.Configuration.getVersion());
        // 设置模板路径 toURI()防止路径出现空格
        /*String classpath = this.getClass().getResource("/").toURI().getPath();*/
        configuration.setClassLoaderForTemplateLoading(getClass().getClassLoader(),
                properties.getFreemarker().getBasePackagePath());
        // 设置字符集
        configuration.setDefaultEncoding("utf-8");
        return configuration;
    }

}
