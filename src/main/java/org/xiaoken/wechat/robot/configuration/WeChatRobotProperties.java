package org.xiaoken.wechat.robot.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * 插件配置属性
 *
 * @author guokenye
 */
@ConfigurationProperties(prefix = "wechat.robot")
public class WeChatRobotProperties {
    /**
     * 是否启用该插件配置
     */
    private boolean enable;
    private Integer socketTimeout;
    private Integer connectTimeout;
    private FreemarkerProperties freemarker;
    private String senderNameSuffix;
    private Map<String, String> senderMap;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Integer getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(Integer socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public Integer getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(Integer connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public FreemarkerProperties getFreemarker() {
        return freemarker;
    }

    public void setFreemarker(FreemarkerProperties freemarker) {
        this.freemarker = freemarker;
    }

    public String getSenderNameSuffix() {
        return senderNameSuffix;
    }

    public void setSenderNameSuffix(String senderNameSuffix) {
        this.senderNameSuffix = senderNameSuffix;
    }

    public Map<String, String> getSenderMap() {
        return senderMap;
    }

    public void setSenderMap(Map<String, String> senderMap) {
        this.senderMap = senderMap;
    }
}
