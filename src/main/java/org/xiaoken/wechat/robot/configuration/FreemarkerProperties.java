package org.xiaoken.wechat.robot.configuration;

/**
 * freeMarker模板配置属性
 * @author guokenye
 */
public class FreemarkerProperties {
    private String basePackagePath;

    public String getBasePackagePath() {
        return basePackagePath;
    }

    public void setBasePackagePath(String basePackagePath) {
        this.basePackagePath = basePackagePath;
    }
}
