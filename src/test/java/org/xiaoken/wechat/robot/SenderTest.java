package org.xiaoken.wechat.robot;

import freemarker.template.TemplateException;
import org.xiaoken.wechat.robot.message.MessageException;
import org.xiaoken.wechat.robot.message.Sender;
import org.xiaoken.wechat.robot.message.impl.MarkDownMessage;
import org.xiaoken.wechat.robot.message.impl.MarkDownTemplateMessage;
import org.xiaoken.wechat.robot.message.impl.TextMessage;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SenderTest {

    @Autowired
    private Sender defaultSender;
    @Resource(name="dailySender")
    private Sender dailySender;



    @org.junit.Test
    public void testText() throws IOException, MessageException, TemplateException {
        TextMessage textMsg = new TextMessage("测试用例");
        defaultSender.send(textMsg);
        Map<String,Object> param=new HashMap<>();
        param.put("targetDate","2019-12-30");
        MarkDownTemplateMessage templateMsg=new MarkDownTemplateMessage(param,"test.ftl");
        defaultSender.send(templateMsg);

        MarkDownMessage markDownMessage=new MarkDownMessage();
        markDownMessage.getBuilder().addTitle("添加标题",2).addNewLine().addInfoStr("测试Info");
        defaultSender.send(markDownMessage);



        dailySender.send(textMsg);


    }


}
